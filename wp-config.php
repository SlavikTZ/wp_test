<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'slava-wp-test');

/** Имя пользователя MySQL */
define('DB_USER', 'slava-lysenko');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'vce6sokq');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8Z+~fD{^aXnm(8Rq8P,_{Apea6{&u/,{q2}U#([]B~SQS&,FpLII-eJ=Slb)wl*f');
define('SECURE_AUTH_KEY',  '8FQb87xX{Wz;BadW9H-B&Rl:aXBL~PLP1%{$fDI|ae<{2tB<$OlYCQ:#+EpMtHs]');
define('LOGGED_IN_KEY',    'EC>br2b CeZPWWzDPx$7X22paRXryHWL|*@td*>.&]RQhH:{}4DA?jz$zD>P8R6E');
define('NONCE_KEY',        ';HbLhqO^s;Q]:yVVo?t5P!b9U3d6K{ZD)(jYTIqA(%!w}&+tXu2X{^/Z:FpW8zV7');
define('AUTH_SALT',        'z  <M^eojY[>.)^g=UTZp8t_KRpk1)a_PH!P0N)sQXW2)/tQ#P)&KZfkk=PN$-I|');
define('SECURE_AUTH_SALT', 'k>4%_S)N=( Kis$B?TW-v`o;44sRWkZJClwIdkeexT=n%Ds}zAC3}N$3bq;k(0qF');
define('LOGGED_IN_SALT',   '2~}Uz7S6z1T_0)6<{8[2ITgiT+6up,.C<KRq@7]j_LzFFq592<!yI{[.#E9H&OLg');
define('NONCE_SALT',       'yC8QUN$Exog{&m+1j:@AP20<g{hlz/54Y4gTfkQxE%92BqJQng0zj7r@vzd<?(za');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
